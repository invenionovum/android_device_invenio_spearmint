# Inherit from the common Open Source product configuration
$(call inherit-product, $(SRC_TARGET_DIR)/product/generic_no_telephony.mk)

# Inherit from hardware-specific part of the product configuration
$(call inherit-product, device/invenio/spearmint/device.mk)

# Release name
PRODUCT_RELEASE_NAME := invenios

PRODUCT_DEVICE := spearmint
PRODUCT_NAME := invenios_spearmint

PRODUCT_BRAND := Invenio
PRODUCT_MODEL := spearmint
PRODUCT_MANUFACTURER := Invenio
PRODUCT_RESTRICT_VENDOR_FILES := false

# Boot animation
TARGET_SCREEN_HEIGHT      := 400
TARGET_SCREEN_WIDTH       := 400
TARGET_BOOTANIMATION_NAME := 400
